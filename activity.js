// num 3
db.rooms.insertOne(
	{
		"name": "single",
		"accomodates": 2,
		"price": 1000,
		"description" : "A single room with all the basic necessities",
		"rooms_available" : 10,
		"isAvailable": false
	}
)

//num 4
db.rooms.insertMany([
	{
		"name": "double",
		"accomodates": 3,
		"price": 2000,
		"description" : "A room fit for a small family going on a vacation",
		"rooms_available" : 5,
		"isAvailable": false
	},
	{
		"name": "queen",
		"accomodates": 4,
		"price": 4000,
		"description" : "A room with a queen sized bed perfect for a simple getaway",
		"rooms_available" : 15,
		"isAvailable": false
	}
])

//num 5 - find double room
db.rooms.find({"name" : "double"});

//num 6 - update queen's available room to 0
db.rooms.updateOne({"rooms_available" : 15}, {$set : {"rooms_available" : 0}})

//num 7 - delete all rooms that have 0 availability
db.rooms.deleteMany({"rooms_available" : 0})
