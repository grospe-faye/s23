//CRUD
//Create, Read, Update, Delete

// Create
// allows us to create documents under a collection or create a collection if it does not exist yet
/*
	Syntax
	db.collections.insertOne({})
		allows to insert or create one document
		collections - stands for collection name
*/

db.users.insertOne(
	{
		"firstName": "Tony",
		"lastName": "Stark",
		"username": "iAmIronMan",
		"email": "iloveyou3000@gmail.com",
		"password": "starkIndustries",
		"isAdmin": true
	}
)

// db.collections.insertMany([{}, {}])
// allows us to insert or create two or more documents

db.users.insertMany([
	{
		"firstName": "Pepper",
		"lastName": "Potts",
		"username": "rescueArmor",
		"email": "pepper@mail.com",
		"password": "whereIsTonyAgain",
		"isAdmin": false
	},
	{
		"firstName": "Steve",
		"lastName": "Rogers",
		"username": "theCaptain",
		"email": "captAmerica@mail.com",
		"password": "iCanLiftMjolnirToo",
		"isAdmin": false	
	},
	{
		"firstName": "Thor",
		"lastName": "Odinson",
		"username": "mightyThor",
		"email": "ThorNotLoki@mail.com",
		"password": "iAmWorthyToo",
		"isAdmin": false
	},
	{
		"firstName": "Loki",
		"lastName": "Odinson",
		"username": "godOfMischief",
		"email": "loki@mail.com",
		"password": "iAmReallyLoki",
		"isAdmin": false
	}

])

//mini-activity
/*
	Make new collection with the name courses
	Insert the ff fields and values:
		name: Javascript,
		price:3500,
		description: Learn Javascript in a week!,
		isActive: true

		name: HTML,
		price:1000,
		description: Learn Basic HTML in 3 days!,
		isActive: true

		name: CSS,
		price:2000,
		description: Make your website fancy, learn CSS now!,
		isActive: true
*/
db.courses.insertMany([
	{
		"name": "Javascript",
		"price": 3500,
		"description": "Learn Javascript in a week!",
		"isActive": true
	},
	{
		"name": "HTML",
		"price": 1000,
		"description": "Learn Basic HTML in 3 days!",
		"isActive": true
	},
	{
		"name": "CSS",
		"price": 2000,
		"description": "Make your website fancy, learn CSS now!",
		"isActive": true
	}

])

// Read
// allows us to retrieve data
// it needs a query or filters to specify the document we are retrieving

// Syntax
	// db.collections.find()
		// allows us to retrieve all documents in the collection

db.users.find();

//Syntax
	//db.collections.find({criteria: value})
	// allows us to find the document that matches our criteria

db.users.find({"isAdmin" : false});

//Syntax
	//db.collections.find({}) - all

db.collections.findOne({})
//allows to find the first document.

//Syntax
	// db.collections.find({,})
	// allows us to find the document that satisfy all criterias

db.users.find({"lastName" : "Odinson" , "firstName" : "Loki"})

// Update
//allows to update documents.
//also use  criteria or filter
//$set operator
//Reminder: updates are permanent and can't be rolled back

//Syntax:
	//db.collections.updateOne({criteria : value}, {$set: {"fieldToBeUpdated" : "updatedValue"}}) 
	// allows us to update one document that satisfy the criteria

db.users.updateOne({"lastName" : "Pots"}, {$set : {"lastName" : "Stark"}})

//Syntax:
	//db.collections.updateMany({criteria: value}, {set: {fieldToBeUpdated: updatedValue}})
	//allows us to update All document that satisfy the criteria

db.users.updateMany({"lastName" : "Odinson"}, {$set : {"isAdmin" : true}})

//Syntax:
	// db.collections.update({}, {$set : {fieldToBeUpdated : updatedValue}})
	//allows us to update the first item in the collection

db.users.updateOne({}, {$set : {"email" : "starkindustries@mail.com"}})

// mini-activity
// update the Javascript course
// make isActive: false

db.courses.updateOne({"name" : "Javascript"}, {$set : {"isActive" : false}})

db.courses.updateMany({}, {$set : {"enrollees" : 10}})
//$set if there is no field, it will add a field

// Delete
// allows us to delete documents
//provide criteria or filters to specify which document to delete from the collection
//Reminder: Be careful when deleting documents, because it will be complicated to retrieve them back again.

//Syntax
	// db.collections.deleteOne({criteria : value})
	// allows us to delete the first itme that matches our criteria

db.users.deleteOne({"isAdmin" : false})

//Syntax
	// db.collections.deleteMany({criteria : value})
	// allows us to delete all items that matches our criteria

db.users.deleteMany({"lastName" : "Odinson"})

// Syntax
	// db.collections.deleteOne({})
	// allows us to delete the first document

db.users.deleteOne({})

//Syntax
	//db.collections.deleteMany({})
	// allows us to delete all items in the collections.

db.users.deleteMany({})